﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rpc
{
    public class JsonRpcRequest<RequestType>
    {
        [JsonProperty("jsonrpc")]
        public string Version { get; set; } = "2.0";
        [JsonProperty("method")]
        public string Method { get; set; }
        [JsonProperty("id")]
        public long? Identifier { get; set; } = null;
        [JsonProperty("params")]
        public RequestType Parameters { get; set; } = default(RequestType);
    }

    public struct JsonRpcError
    {
        [JsonProperty("code")]
        public long Code { get; set; }
        [JsonProperty("Message")]
        public string Message { get; set; }

        public override string ToString() => Code.ToString();
    }

    public class JsonRpcResponse<ResponseType>
    {
        [JsonProperty("jsonrpc")]
        public string Version { get; set; } = "2.0";
        [JsonProperty("error")]
        public JsonRpcError? Error { get; set; }
        [JsonProperty("id")]
        public long? Identifier { get; set; } = null;
        [JsonProperty("result")]
        public ResponseType Result { get; set; } = default(ResponseType);
    }
}

﻿using NLog;
using Rpc.Payload;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rpc.Services
{
    public class Explorer
    {
        private readonly Client RemoteClient;
        private readonly ILogger Logger;

        public Explorer(Client client, ILogger logger)
        {
            RemoteClient = client ?? throw new ArgumentNullException(nameof(client));
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<ShortBlockInfo> TopBlockInfoShort()
        {
            return await RemoteClient.Request<ShortBlockInfo>("explorer.block.short");
        }

        public async Task<ShortBlockInfo> TopBlockInfoShort(uint height)
        {
            return await RemoteClient.Request<Variant, ShortBlockInfo>("explorer.block.short", new Variant { type = "height", value = height });
        }

        public async Task<ShortBlockInfo> TopBlockInfoShort(string hash)
        {
            return await RemoteClient.Request<Variant, ShortBlockInfo>("explorer.block.short", new Variant { type = "hash", value = hash });
        }

        public async Task<BlockInfo> TopBlockInfo()
        {
            return await RemoteClient.Request<BlockInfo>("explorer.block");
        }

        public async Task<BlockInfo> TopBlockInfo(uint height)
        {
            return await RemoteClient.Request<Variant, BlockInfo>("explorer.block", new Variant { type = "height", value = height });
        }

        public async Task<BlockInfo> TopBlockInfo(string hash)
        {
            return await RemoteClient.Request<Variant, BlockInfo>("explorer.block", new Variant { type = "hash", value = hash });
        }

        public async Task<ShortBlockInfo[]> ShortBlockInfos(uint[] heights)
        {
            return await RemoteClient.Request<Variant, ShortBlockInfo[]>("explorer.block.batch.short", new Variant { type = "height", value = heights });
        }

        public async Task<TransactionInfo> TransactionInfo(string hash)
        {
            return await RemoteClient.Request<string, TransactionInfo>("explorer.transaction", hash);
        }

        public async Task<ShortPoolInfo> ShortPoolInfo()
        {
            return await RemoteClient.Request<ShortPoolInfo>("explorer.pool");
        }

        public async Task<PoolInfo> PoolInfo()
        {
            return await RemoteClient.Request<PoolInfo>("explorer.pool.detailed");
        }
    }
}

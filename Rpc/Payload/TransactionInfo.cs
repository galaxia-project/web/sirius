﻿using Newtonsoft.Json;
using Rpc.Payload.Converter;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Rpc.Payload
{
    public enum TransactionType
    {
        [EnumMember(Value = "block_reward")]
        BlockReward,

        [EnumMember(Value = "static_reward")]
        StaticReweard,

        [EnumMember(Value = "transfer")]
        Transfer,

        [EnumMember(Value = "fusion")]
        Fusion,
    };

    public enum TransactionContainer
    {
        [EnumMember(Value = "main_chain")]
        MainChain = 1,

        [EnumMember(Value = "alternative_chain")]
        AlternativeChain,

        [EnumMember(Value = "pool")]
        Pool,
    };

    public class ShortTransactionInfo
    {
        [JsonProperty("type")]
        public TransactionType Type { get; set; }

        [JsonProperty("container")]
        public TransactionContainer Container { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("fee")]
        public ulong Fee { get; set; }

        [JsonProperty("sum_input")]
        public ulong SumInput { get; set; }

        [JsonProperty("sum_output")]
        public ulong SumOutput { get; set; }

        [JsonProperty("unlock_time")]
        public ulong Unlock { get; set; }

        [JsonProperty("blob_size")]
        public ulong BlobSize { get; set; }
    };

    public class TransactionExtraInfo
    {
        [JsonProperty("raw")]
        public string Raw { get; set; }

        [JsonProperty("public_key")]
        public string PublicKey { get; set; }

        [JsonProperty("payment_id")]
        public string PaymentId { get; set; }
    }

    public class ShortTransactionBaseInputInfo
    {
        [JsonProperty("height")]
        public uint Height { get; set; }
    };

    public struct ShortTransactionKeyInputInfo
    {
        [JsonProperty("amount")]
        public ulong Amount { get; set; }

        [JsonProperty("ring_size")]
        public ulong RingSize { get; set; }
    };

    public struct ShortTransactionKeyOutputInfo
    {
        [JsonProperty("amount")]
        public ulong Amount { get; set; }
    };

    public struct ShortTransactionInputInfo
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public ShortTransactionBaseInputInfo BaseInput { get; set; }

        [JsonProperty("value")]
        public ShortTransactionKeyInputInfo KeyInput { get; set; }

        public bool ShouldSerializeBaseInput() => Type == "base";
        public bool ShouldSerializeKeyInput() => Type == "key";
    }

    public struct ShortTransactionOutputInfo
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public ShortTransactionKeyInputInfo KeyOutput { get; set; }
        public bool ShouldSerializeKeyOutput() => Type == "key";
    }

    public class TransactionInfo : ShortTransactionInfo
    {
        [JsonProperty("extra")]
        public TransactionExtraInfo Extra { get; set; }

        //[JsonProperty("inputs")]
        //public ShortTransactionInputInfo[] Inputs { get; set; }

        //[JsonProperty("outputs")]
        //public ShortTransactionOutputInfo[] Outputs { get; set; }

        [JsonProperty("block")]
        public ShortBlockInfo Block { get; set; }
    }
}

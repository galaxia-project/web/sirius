﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rpc.Payload
{
    public class ShortPoolInfo
    {
        [JsonProperty("state_hash")]
        public string State { get; set; }

        [JsonProperty("count")]
        public ulong Count { get; set; }

        [JsonProperty("cumulative_size")]
        public ulong Size { get; set; }

        [JsonProperty("cumulative_fees")]
        public ulong Fees { get; set; }
    }

    public class PoolInfo : ShortPoolInfo
    {
        [JsonProperty("transactions")]
        public ShortTransactionInfo[] Transactions { get; set; }
    }
}

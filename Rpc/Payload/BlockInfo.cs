﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Rpc.Payload.Converter;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Rpc.Payload
{
    public enum BlockSource
    {
        [EnumMember(Value = "main_chain")]
        MainChain,

        [EnumMember(Value = "alternative_chain")]
        AlternativeChain
    }

    public class ShortBlockInfo
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("height")]
        public uint Height { get; set; }

        [JsonProperty("timestamp")]
        [JsonConverter(typeof(SecondsEpochConverter))]
        public DateTime Timestamp { get; set; }

        [JsonProperty("chain")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BlockSource Chain { get; set; }

        [JsonProperty("blob_size")]
        public ulong BlobSize { get; set; }

        [JsonProperty("version")]
        public uint Version { get; set; }

        [JsonProperty("upgrade_vote")]
        public uint UpgradeVote { get; set; }

        [JsonProperty("cumulative_difficulty")]
        public ulong CumulativeDifficulty { get; set; }

        [JsonProperty("cumulative_supply")]
        public ulong CumulativeSupply { get; set; }

        [JsonProperty("cumulative_transactions_count")]
        public ulong CumulativeTransactionsCount { get; set; }
    }

    public class BlockInfo : ShortBlockInfo
    {
        [JsonProperty("nonce")]
        public string Nonce { get; set; }

        [JsonProperty("previous_hash")]
        public string PreviousHash { get; set; }

        [JsonProperty("transactions_count")]
        public ushort TransactionCount { get; set; }

        [JsonProperty("miner_reward")]
        public ShortTransactionInfo MinerReward { get; set; }

        [JsonProperty("static_reward")]
        public ShortTransactionInfo StaticReward { get; set; }

        [JsonProperty("transfers")]
        public ShortTransactionInfo[] Transfers { get; set; }

        [JsonProperty("sum_fees")]
        public ulong SumFees { get; set; }
    }
}

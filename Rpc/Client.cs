﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Rpc
{
    public class Client
    {
        private readonly ILogger Logger;
        private readonly Uri Host;
        private readonly Uri JsonRpcEndpoint;
        private readonly IConfigurationRoot Config;

        public Client(IConfigurationRoot config, ILogger logger)
        {
            Config = config ?? throw new ArgumentNullException(nameof(config));
            Host = new Uri(Config["daemon:host"] ?? throw new ArgumentNullException("config:daemon:host"));
            JsonRpcEndpoint = new Uri(Host, "/rpc");
            Logger = logger ?? throw new ArgumentNullException();
        }

        public async Task<ResponseType> Request<RequestType, ResponseType>(string method, RequestType request){
            try
            {
                HttpClient client = new HttpClient();
                JsonRpcRequest<RequestType> jsonRequest = new JsonRpcRequest<RequestType>
                {
                    Version = "2.0",
                    Identifier = null,
                    Method = method,
                    Parameters = request
                };
                string payload = JsonConvert.SerializeObject(jsonRequest, Formatting.None);

                var rawResponse = await client.PostAsync(JsonRpcEndpoint, new StringContent(payload, Encoding.ASCII, "application/json"));
                if (!rawResponse.IsSuccessStatusCode)
                {
                    Logger.Error("JsonRpc invocation '{}' failed with error code: {}", method, rawResponse.StatusCode);
                    throw new HttpRequestException("None success response code.");
                }
                string content = await rawResponse.Content.ReadAsStringAsync();
                JsonRpcResponse<ResponseType> jsonResponse = JsonConvert.DeserializeObject<JsonRpcResponse<ResponseType>>(content);
                if (jsonResponse.Error != null)
                {
                    Logger.Error("JsonRpc invocation '{}' failed with error code: {}", method, jsonResponse.Error);
                    throw new HttpRequestException("None success response code.");
                }
                return jsonResponse.Result;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        public async Task<ResponseType> Request<ResponseType>(string method)
        {
            return await Request<object, ResponseType>(method, null);
        }
    }
}

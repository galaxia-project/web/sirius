﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Nemi.Extensions.Configuration.Yaml;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Discord.Addons.Interactive;

namespace Sirius
{
    class Sirius
    {
        public IConfigurationRoot Configuration { get; private set; }

        private readonly ILogger Logger;

        public Sirius(ILogger logger = null)
        {
            Logger = logger ?? LogManager.GetLogger("SIRIUS");
        }

        private Task OnClientReady()
        {
            Logger.Info("Service is ready.");
            return Task.CompletedTask;
        }

        public Task Initialize(string[] args)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddYamlFile("config.yaml")
                .Build();
            return Task.CompletedTask;
        }

        public async Task Run()
        {
            ServiceCollection services = new ServiceCollection();
            await ConfigureServices(services);

            ServiceProvider provider = services.BuildServiceProvider();

            provider.GetRequiredService<Services.Log>();
            provider.GetRequiredService<Services.CommandHandler>();

            await provider.GetRequiredService<Services.Startup>().StartAsync();
            await WebHost.CreateDefaultBuilder()
                .UseStartup<FileServer>()
                .ConfigureServices(config =>
                {
                    config
                        .AddSingleton(Configuration)
                        .AddSingleton(Logger)
                    ;
                })
                .UseUrls($"http://{Configuration["fileserver:local-bind"]}:{Configuration["fileserver:port"]}/")
                .Build()
                .RunAsync();
        }

        private Task ConfigureServices(ServiceCollection services)
        {
            services.AddSingleton(
                new DiscordSocketClient(
                new DiscordSocketConfig
                {
                    LogLevel = LogSeverity.Verbose,
                    MessageCacheSize = 16
                })
            )
            .AddSingleton(
                new CommandService(
                new CommandServiceConfig
                {
                    LogLevel = LogSeverity.Verbose,
                    DefaultRunMode = RunMode.Async
                }
            ))
            .AddSingleton<Services.CommandHandler>()
            .AddSingleton<Services.Startup>()
            .AddSingleton<Services.Log>()
            .AddSingleton<Services.FileProvider>()
            .AddSingleton<InteractiveService>()
            .AddSingleton<Formatter>()
            .AddSingleton<Rpc.Client>()
            .AddSingleton<Rpc.Services.Explorer>()
            .AddSingleton(Logger)
            .AddSingleton(Configuration)
            ;
            return Task.CompletedTask;
        }
    }
}

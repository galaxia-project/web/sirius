﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sirius
{
    public class FileServer
    {
        private readonly IConfigurationSection Config;
        private readonly ILogger Logger;

        public FileServer(IConfigurationRoot config, ILogger logger)
        {
            Config = config?.GetSection("fileserver") ?? throw new ArgumentNullException(nameof(config));
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Configure(IApplicationBuilder app)
        {
            string root = Path.GetFullPath(Config["root"]);
            Directory.CreateDirectory(root);
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(root),
                RequestPath = "/files"
            });
        }
    }
}

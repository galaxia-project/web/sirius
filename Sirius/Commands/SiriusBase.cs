﻿using Discord;
using Discord.Addons.Interactive;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Commands
{
    public class SiriusModule<T> : InteractiveBase<SocketCommandContext>
    {
        public async Task ReplyAsync(Embed[] embeds)
        {
            if((embeds?.Length ?? 0) == 0)
            {
                return;
            }

            if(embeds.Length == 1)
            {
                await ReplyAsync(embed: embeds[0]);
            }
            else
            {
                var pages = embeds.Select(embed =>
                {
                    return new PaginatedMessage.Page()
                    {
                        Fields = embed.Fields.Select(f => 
                            new EmbedFieldBuilder()
                                .WithName(f.Name)
                                .WithValue(f.Value)
                                .WithIsInline(f.Inline))
                        .ToList(),
                        Color = embed.Color,
                        ImageUrl = embed.Image?.Url,
                        ThumbnailUrl = embed.Thumbnail?.Url,
                        TimeStamp = embed.Timestamp
                    };
                });

                var pager = new PaginatedMessage
                {
                    Pages = pages
                };


                await PagedReplyAsync(pager, new ReactionList
                {
                    Forward = true,
                    Backward = true,
                    Jump = true,
                    Trash = true
                });
            }
        }
    }
}

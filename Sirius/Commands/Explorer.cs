﻿using Discord;
using Discord.Addons.Interactive;
using Discord.Commands;
using Newtonsoft.Json;
using Sirius.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Commands
{
    [Group("explorer")]
    [Name(":tv: Explorer")]
    [Alias("e")]
    public class ExplorerModule : SiriusModule<SocketCommandContext>
    {
        private readonly Rpc.Services.Explorer Remote;
        private readonly Formatter Formatter;
        private readonly FileProvider Files;

        public ExplorerModule(Rpc.Services.Explorer remote, Formatter formatter, FileProvider files)
        {
            Remote = remote ?? throw new ArgumentNullException(nameof(remote));
            Formatter = formatter ?? throw new ArgumentNullException(nameof(formatter));
            Files = files ?? throw new ArgumentNullException(nameof(files));
        }

        [Command("")]
        [Alias("s", "status")]
        [Priority(0)]
        public async Task Status()
        {
            await Top();
        }

        [Command("")]
        [Alias("s", "search")]
        [Summary("Searches for a hash or height.")]
        [Priority(5)]
        public async Task Search(string search)
        {
            uint height = 0;
            if(uint.TryParse(search, out height))
            {
                await Block(height);
            }
            else
            {
                var block = await Remote.TopBlockInfo(search);
                if(block != null)
                {
                    await ReplyAsync(Formatter.Format(block));
                    return;
                }

                var transaciton = await Remote.TransactionInfo(search);
                if(transaciton != null)
                {
                    await ReplyAsync(embed: Formatter.Format(transaciton));
                    return;
                }
            }
        }

        [Command("top")]
        [Alias("t")]
        [Priority(10)]
        [Summary("Queries top block info.")]
        public async Task Top()
        {
            var top = await Remote.TopBlockInfo();
            await ReplyAsync(Formatter.Format(top));
        }

        [Command("block")]
        [Alias("b")]
        [Priority(20)]
        [Summary("Queries block info for a specific height.")]
        public async Task Block(uint height)
        {
            var block = await Remote.TopBlockInfo(height);
            await ReplyAsync(Formatter.Format(block));
        }

        [Command("block")]
        [Alias("b")]
        [Priority(10)]
        [Summary("Queries block info for a specific hash.")]
        public async Task Block(string hash)
        {
            var block = await Remote.TopBlockInfo(hash);
            await ReplyAsync(Formatter.Format(block));
        }

        [Command("transaction")]
        [Alias("t", "tx")]
        [Priority(10)]
        [Summary("Queries transaction info for a specific hash.")]
        public async Task Transaction(string hash)
        {
            var tx = await Remote.TransactionInfo(hash);
            await ReplyAsync(embed: Formatter.Format(tx));
        }

        [Command("pool")]
        [Alias("p")]
        [Priority(10)]
        [Summary("Displays current pool state.")]
        public async Task Pool()
        {
            var pool = await Remote.PoolInfo();
            await ReplyAsync(Formatter.Format(pool));
        }

        [Command("checkpoints")]
        [Alias("cp")]
        [Priority(10)]
        [Summary("Generates a new checkpoints export.")]
        public async Task Checkpoints()
        {
            const uint minDensity = 5000;
            const uint topOffset = 200;
            const uint density = 200;

            var top = await Remote.TopBlockInfo();
            
            var startHeight = (top.Height / topOffset) * topOffset;
            if(top.Height - startHeight  < topOffset)
            {
                startHeight -= topOffset;
            }

            var startBlock = await Remote.ShortBlockInfos(new[] { startHeight });
            var uri = await Files.GetCachedResource("checkpoints", $"{startBlock.First().Hash}.csv", async writer =>
            {
                uint currentDensity = density;
                while (startHeight > currentDensity)
                {
                    List<uint> heights = new List<uint>();
                    for (int i = 0; i < 30 && startHeight > currentDensity; ++i, startHeight -= density)
                    {
                        heights.Add(startHeight);
                    }

                    var blocks = await Remote.ShortBlockInfos(heights.ToArray());
                    foreach (var block in blocks)
                    {
                        await writer.WriteLineAsync($"{block.Height - 1},{block.Hash}");
                    }

                    currentDensity = (currentDensity * 14) / 10;
                    if (currentDensity > minDensity)
                    {
                        currentDensity = minDensity;
                    }
                }
            });

            await ReplyAsync(embed: Formatter.Format(uri, $"[{startBlock.Last().Height}]: {startBlock.Last().Hash}", Icon.Checkpoints));
        }
    }
}

﻿using Discord;
using Microsoft.Extensions.Configuration;
using NLog;
using Rpc.Payload;
using Sirius.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sirius
{
    public class Formatter
    {
        private readonly IConfigurationRoot Config;
        private readonly FileProvider Files;
        private readonly ILogger Logger;
        private Uri WebExplorerRoot {  get { return new Uri(Config["web:explorer"]); } }

        private Uri GetEntityWebUrl(string id)
        {
            return new Uri(WebExplorerRoot, string.Format("detail/{0}", id));
        }

        private string AsCoins(ulong coins)
        {
            decimal fraction = coins / ((decimal)Math.Pow(10, 6));
            return string.Format("{0:0.000000} GLX", fraction);
        }

        private string AsEntity(string id)
        {
            return $"[{id}]({GetEntityWebUrl(id)})";
        }

        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        private string AsBlobSize(ulong value, int decimalPlaces = 2)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        public Formatter(IConfigurationRoot config, FileProvider files, ILogger logger)
        {
            Config = config ?? throw new ArgumentNullException(nameof(config));
            Files = files ?? throw new ArgumentNullException(nameof(files));
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public List<Embed> Format(ShortTransactionInfo[] transactions, TransactionContainer container)
        {

            try
            {
                List<Embed> reval = new List<Embed>();
                const int maxTransfers = 5;
                for (int i = 0; i < transactions.Length; i += maxTransfers)
                {
                    EmbedBuilder batchBuilder = new EmbedBuilder();
                    batchBuilder.Color = GetColor(container);
                    List<string> transfers = new List<string>();
                    for (int j = 0; j < maxTransfers && (i * maxTransfers) + j < transactions.Length; ++j)
                    {
                        transfers.Add(AsEntity(transactions[(i * maxTransfers) + j].Hash));
                    }
                    if(transfers.Count > 0)
                    {
                        batchBuilder.AddField("Transfers", string.Join('\n', transfers));
                        reval.Add(batchBuilder.Build());
                    }
                }

                return reval;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        public Embed[] Format(BlockInfo info)
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.ThumbnailUrl = Files.GetIcon(Icon.Block);
            builder.Url = GetEntityWebUrl(info.Hash).ToString();
            builder.Title = info.Hash;
            builder.Timestamp = info.Timestamp;
            builder.Color = GetColor(info.Chain);
            builder.AddField(nameof(info.Height), info.Height, true);
            builder.AddField(nameof(info.Chain), info.Chain, true);
            builder.AddField("Size", AsBlobSize(info.BlobSize), true);
            builder.AddField("Difficulty", info.CumulativeDifficulty, true);
            builder.AddField("Transactions", info.CumulativeTransactionsCount, true);
            builder.AddField("Supply", AsCoins(info.CumulativeSupply), true);
            builder.AddField(nameof(info.MinerReward), AsEntity(info.MinerReward.Hash));
            if(info.StaticReward != null)
            {
                builder.AddField(nameof(info.StaticReward), AsEntity(info.StaticReward.Hash));
            }

            if((info.Transfers?.Length ?? 0) > 0)
            {
                var transfers = Format(info.Transfers, info.Chain == BlockSource.MainChain ? TransactionContainer.MainChain : TransactionContainer.AlternativeChain);
                transfers.Insert(0, builder.Build());
                return transfers.ToArray();
            }
            else
            {
                return new[] { builder.Build() };
            }
        }

        private Color GetColor(BlockSource chain)
        {
            switch (chain)
            {
                case BlockSource.MainChain:
                    return Color.Green;
                case BlockSource.AlternativeChain:
                    return Color.Orange;
                default:
                    return Color.Red;
            }
        }

        public Icon GetIcon(TransactionType type)
        {
            switch (type)
            {
                case TransactionType.BlockReward:
                    return Icon.Reward;
                case TransactionType.StaticReweard:
                    return Icon.StaticReward;
                case TransactionType.Transfer:
                    return Icon.Transfer;
                case TransactionType.Fusion:
                    return Icon.Fusion;
                default:
                    return Icon.Missing;
            }
        }

        public Color GetColor(TransactionContainer container)
        {
            switch (container)
            {
                case TransactionContainer.MainChain:
                    return Color.Green;
                case TransactionContainer.AlternativeChain:
                    return Color.Red;
                case TransactionContainer.Pool:
                    return Color.LightGrey;
                default:
                    return Color.LightGrey;
            }
        }

        public Embed Format(ShortTransactionInfo info)
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.ThumbnailUrl = Files.GetIcon(GetIcon(info.Type));
            builder.Url = GetEntityWebUrl(info.Hash).ToString();
            builder.Title = info.Hash;
            builder.Color = GetColor(info.Container);
            builder.AddField(nameof(info.Container), info.Container.ToString(), true);
            builder.AddField("Size", AsBlobSize(info.BlobSize), true);

            if (info.Type == TransactionType.Fusion)
            {
                builder.AddField("Amount", AsCoins(info.SumInput));
            }
            else if(info.Type == TransactionType.Transfer)
            {
                builder.AddField("Input", AsCoins(info.SumInput), true);
                builder.AddField("Output", AsCoins(info.SumOutput), true);
                builder.AddField("Fee", AsCoins(info.Fee), true);
            }
            else
            {
                builder.AddField("Reward", AsCoins(info.SumOutput), true);
            }

            if(info.Unlock > 0)
            {
                builder.AddField(nameof(info.Unlock), info.Unlock, true);
            }

            return builder.Build();
        }

        public Embed Format(ShortPoolInfo info)
        {
            EmbedBuilder builder = new EmbedBuilder();

            builder.Title = string.Format("Pool: {0}", info.State);
            builder.Url = WebExplorerRoot.ToString();
            builder.AddField(nameof(info.Size), AsBlobSize(info.Size), true);
            builder.AddField(nameof(info.Fees), AsCoins(info.Fees), true);

            return builder.Build();
        }

        public Embed[] Format(PoolInfo info)
        {
            var summary = Format(info as ShortPoolInfo);
            if((info.Transactions?.Length ?? 0) == 0)
            {
                return new[] { summary };
            }
            else
            {
                var transfers = Format(info.Transactions, TransactionContainer.Pool);
                transfers.Insert(0, summary);
                return transfers.ToArray();
            }
        }

        public Embed Format(string uri, string id, Icon? icon = null)
        {
            EmbedBuilder builder = new EmbedBuilder();

            builder.Title = id;
            builder.Url = uri;
            if(icon != null)
            {
                builder.ThumbnailUrl = Files.GetIcon(icon ?? Icon.Missing);
            }
            return builder.Build();
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Services
{
    [System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    sealed class FileNameAttribute : Attribute
    {
        public string Value { get; set; }
    }

    public enum Icon
    {
        [FileName(Value = "missing")]
        Missing,
        [FileName(Value = "block")]
        Block,
        [FileName(Value = "reward")]
        Reward,
        [FileName(Value = "static_reward")]
        StaticReward,
        [FileName(Value = "fusion")]
        Fusion,
        [FileName(Value = "transfer")]
        Transfer,
        [FileName(Value = "checkpoints")]
        Checkpoints
    }

    public class FileProvider
    {
        private readonly IConfiguration Config;
        private string Cache => Path.Combine(Config["root"], "cache");
        private string GetCacheDirectory(string dir) => Path.Combine(Cache, dir);
        private string GetCacheFile(string dir, string id) => Path.Combine(GetCacheDirectory(dir), id);
        private string GetCacheFileUri(string dir, string id) => new Uri(RootUri, $"files/cache/{dir}/{id}").ToString();

        private string GetFilename(Enum e)
        {
            Type type = e.GetType();

            MemberInfo[] memInfo = type.GetMember(e.ToString());

            if (memInfo != null && memInfo.Length > 0)

            {

                object[] attrs = memInfo[0].GetCustomAttributes(typeof(FileNameAttribute),
                                                                false);

                if (attrs != null && attrs.Length > 0)

                    return ((FileNameAttribute)attrs[0]).Value;

            }

            return e.ToString();
        }


        public Uri RootUri { get => new Uri($"http://{Config["remote-bind"]}:{Config["port"]}/"); }

        public FileProvider(IConfigurationRoot config)
        {
            Config = config?.GetSection("fileserver") ?? throw new ArgumentNullException(nameof(config));
        }

        public string GetIcon(Icon icon)
        {
            return (new Uri(RootUri, $"files/icons/{GetFilename(icon)}.png")).ToString();
        }

        public async Task<string> GetCachedResource(string dir, string id, Func<StreamWriter, Task> callback)
        {
            Directory.CreateDirectory(GetCacheDirectory(dir));
            string file = GetCacheFile(dir, id);
            if(!File.Exists(file))
            {
                using (var writer = new StreamWriter(file))
                {
                    await callback(writer);
                }
            }
            return GetCacheFileUri(dir, id);
        }
    }
}

﻿using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Services
{
    public class CommandHandler
    {
        private readonly DiscordSocketClient Client;
        private readonly CommandService Commands;
        private readonly IConfigurationRoot Config;
        private readonly IServiceProvider Services;
        private readonly ILogger Logger;

        public CommandHandler(DiscordSocketClient client, CommandService commands, IConfigurationRoot config, IServiceProvider services, ILogger logger)
        {
            Client = client ?? throw new ArgumentNullException(nameof(client));
            Commands = commands ?? throw new ArgumentNullException(nameof(commands));
            Config = config ?? throw new ArgumentNullException(nameof(config));
            Services = services ?? throw new ArgumentNullException(nameof(services));
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));

            Client.MessageReceived += OnMessageReceived;
        }

        private async Task OnMessageReceived(SocketMessage arg)
        {
            SocketUserMessage message = arg as SocketUserMessage;
            if(message == null)
            {
                return;
            }

            if(message.Author.Id == Client.CurrentUser.Id 
                || message.Author.IsBot)
            {
                return;
            }

            SocketCommandContext context = new SocketCommandContext(Client, message);
            if(!context.IsPrivate)
            {
                if (!String.IsNullOrEmpty(Config["channel"]) && context.Channel.Id != ulong.Parse(Config["channel"]))
                {
                    return;
                }
            }

            int argumentPosition = 0;
            if(!message.HasStringPrefix(Config["prefix"], ref argumentPosition))
            {
                return;
            }

            IResult commandExecutionResult = await Commands.ExecuteAsync(context, argumentPosition, Services);
            if(!commandExecutionResult.IsSuccess)
            {
                Logger.Debug("Command execution failed: {}\n\t{}", commandExecutionResult.ErrorReason, context.Message);
                return;
            }
        }
    }
}

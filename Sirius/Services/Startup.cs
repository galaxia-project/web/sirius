﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Services
{
    class Startup
    {
        private readonly IServiceProvider Services;
        private readonly DiscordSocketClient Client;
        private readonly CommandService Commands;
        private readonly IConfigurationRoot Config;

        // DiscordSocketClient, CommandService, and IConfigurationRoot are injected automatically from the IServiceProvider
        public Startup(
            IServiceProvider services,
            DiscordSocketClient client,
            CommandService commands,
            IConfigurationRoot config)
        {
            Client = client ?? throw new ArgumentNullException(nameof(client));
            Commands = commands ?? throw new ArgumentNullException(nameof(commands));
            Config = config ?? throw new ArgumentNullException(nameof(config));
            Services = services ?? throw new ArgumentNullException(nameof(services));

        }

        public async Task StartAsync()
        {
            string discordToken = Config["token"];
            if (string.IsNullOrWhiteSpace(discordToken))
            {
                throw new ArgumentNullException("Please provide a discord token in your config.yaml file.");
            }

            await Client.LoginAsync(TokenType.Bot, discordToken);
            await Client.StartAsync();
            await Commands.AddModulesAsync(Assembly.GetEntryAssembly(), Services);
        }
    }
}
